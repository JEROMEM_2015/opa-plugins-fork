	$(document).ready(function(){
		$('div:empty').remove();
		$('.jquerySubmitFlag').prop('checked', false); // reset jquery submit flag
	;

	$("select").change(function(){
		
        //console.log("dropdown has changed.");
		if($('.jquerySubmitFlag').length) {
			$('.jquerySubmitFlag').prop('checked', true);
			
			// use the search text box ID
			var id = "___cmbinput___" + $(this).attr("id");
			sessionStorage.setItem('controlId', id);
			
			// add timeout to ensure processing in searcheable dropdown is completed
			setTimeout(function() {
				$("#submit").click();
			}, 30);
		}
		
    });
	$('input:radio').change(function () {
		//console.log("radio button has changed.");
		
		if($('.jquerySubmitFlag').length) {
			$('.jquerySubmitFlag').prop('checked', true);

			var id = $(this).attr("id");
			sessionStorage.setItem('controlId', id);
			
			// add timeout to ensure processing in searcheable dropdown is completed
			setTimeout(function() {
				$("#submit").click();
			}, 30);
		}
		
		
		
	 });
	 
	var focusId = sessionStorage.getItem('controlId');
	if (focusId != ""){
	
		// below code has error
		//$('html, body').animate({
		//	scrollTop: $('#'+focusId).offset().top -50
		//}, 2000);

		$('#'+focusId).focus();
	}
	
});
